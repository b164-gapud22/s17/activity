
let students = ['billy', 'john', 'anime'];

// number 3
function addStudent(additionalStudent){
		students.push(additionalStudent);
		console.log(`${additionalStudent} was added to the student's list.`);
};

// number 4
function countStudent(){
	console.log(`There are a total of ${students.length} students enrolled`);
};

// number 5
function printStudent() {
	students.sort();
	students.forEach(function(student){
		console.log(student);
	})
};

// number 6
function findStudent(sortStudent){
	students.filter(function(student){
		if(student.toLowerCase() == sortStudent.toLowerCase()){
			console.log(`${sortStudent} is an Enrollee`)
		}else{
			let filteredStudent = students.filter(function(student){
				student.toLowerCase().includes(sortStudent)
			})
			console.log(filteredStudent)
		}
	})
}

// Others
// addSection()
function addSection(letter){
	let section = students.map(function(student){
		return `${student} - section ${letter}`
	});
	console.log(section);
};


// removeStudent()
function removeStudent(remove){
	let index = students.indexOf(remove);
	students.splice(index,1);
	console.log(`${remove} was removed from the student's list.`);
}